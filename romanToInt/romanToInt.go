package romanToInt

func RomanToInt(s string) int {
	dictionary := map[string]int{
		"I": 1,
		"V": 5,
		"X": 10,
		"L": 50,
		"C": 100,
		"D": 500,
		"M": 1000,
	}
	result := dictionary[string(s[len(s)-1])]
	for i := len(s) - 2; i >= 0; i-- {
		if dictionary[string(s[i])] < dictionary[string(s[i+1])] {
			result -= dictionary[string(s[i])]
		} else {
			result += dictionary[string(s[i])]
		}
	}
	return result
}
