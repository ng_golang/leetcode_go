package validParentheses

import "workspace/components"

func IsValid(s string) bool {
    stack := components.Stack{}
	if len(s) == 0 {
		return false
	}
	m := map[rune] rune{'(':')', '{':'}', '[':']'}
	for _,val := range(s) {
		switch val {
		case '(', '{', '[':
			stack.Push(val)
			
		case ')', '}', ']':
			res, _ :=stack.Peek()
			if !stack.IsEmpty() && m[res] == val {
				stack.Pop()
			}else{
				return false
			}
		
		}
	}
	if !stack.IsEmpty(){
		return false
	}

	return true
}

func main(){

}