package worktest

import (
	"testing"
	"workspace/validParentheses"

	"github.com/stretchr/testify/assert"
)

func TestIsValid(t *testing.T){
	a := assert.New(t)
	a.Equal(true,validParentheses.IsValid("()"))
	a.Equal(true,validParentheses.IsValid("()[]{}"))
	a.Equal(false,validParentheses.IsValid("(]"),"error with values (]")
}