package worktest

import (
	"testing"
	"workspace/romanToInt"

	"github.com/stretchr/testify/assert"
)

func TestRomanToInt(t *testing.T){
	a := assert.New(t)
	a.Equal(3,romanToInt.RomanToInt("III"))
	a.Equal(58,romanToInt.RomanToInt("LVIII"))
	a.Equal(1994,romanToInt.RomanToInt("MCMXCIV"),"error with values MCMXCIV")
}