package containsDuplicate

func ContainsDuplicate(nums []int) bool {
	dict := make(map[int]bool)
	for _, val := range nums {
		_, ok := dict[val]
		if !ok {
			dict[val] = false
		} else {
			return true
		}
	}
	return false
}